﻿using Aplicacion.ApplicationService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplicacion.UI
{
    public partial class FrmLogin : Form
    {

        private LoginAppService service = new LoginAppService();
        public FrmLogin()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void bnIngresar_Click(object sender, EventArgs e)
        {
            try
            {
                service.Login(txtUsuario.Text, txtPassword.Text);
                ((FrmPrincipal)this.MdiParent).ValidarFormulario();
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

    }
}
