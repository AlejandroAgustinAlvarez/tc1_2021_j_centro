﻿using Aplicacion.ApplicationService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplicacion.UI
{
    public partial class FrmPrincipal : Form
    {

        private UsuarioAppService usuarioService;
        private LoginAppService loginService;


        public FrmPrincipal()
        {
            usuarioService = new UsuarioAppService();
            InitializeComponent();
            MostrarUsuario();
            ValidarFormulario();
        }

        public void ValidarFormulario()
        {
            this.tsMnuIngresar.Enabled = LoginAppService.Sesion.Equals(Guid.Empty);
            this.tsMnuDesconectar.Enabled = !LoginAppService.Sesion.Equals(Guid.Empty);
            MostrarUsuario();
        }

        private void MostrarUsuario()
        {
            if(LoginAppService.Sesion.Equals(Guid.Empty))
            {
                tsLblStatus.Text = "Sin conexión, ir a Sesión/Ingresar para establecer una conexión.";
            }
            else
            {
                this.tsLblStatus.Text = usuarioService.ObtenerUsuarioPorId(LoginAppService.Sesion) + " há iniciado sesión";
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void ingresarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmLogin frmLogin = new FrmLogin();
            frmLogin.MdiParent = this;
            frmLogin.Show();            
        }

        private void desconectarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            loginService.CerrarSesion();
            ValidarFormulario();
        }
    }
}
