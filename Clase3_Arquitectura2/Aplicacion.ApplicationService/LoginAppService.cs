﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace Aplicacion.ApplicationService
{
    public class LoginAppService
    {
        private static Guid _sesion;

        public static Guid Sesion 
        {
            get
            {
                return _sesion;
            }
        }

        public void CerrarSesion()
        {
            _sesion = Guid.Empty;
        }

        public void Login(string Nombre, string Password)
        
        {
            if (String.IsNullOrEmpty(Nombre) || String.IsNullOrEmpty(Password))
                throw new Exception("Debe ingresar el Nombre y Password!!");

            SqlConnectionStringBuilder sb = new SqlConnectionStringBuilder();
            sb.InitialCatalog = "TC1_2021_J";
            sb.DataSource = ".";
            sb.IntegratedSecurity = true;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = sb.ConnectionString;
            IDataReader dr = null;
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = "select id_usuario from usuarios where username=@username and password=@password";
                cmd.Parameters.AddWithValue("username", Nombre);
                cmd.Parameters.AddWithValue("password", Password);

                dr = cmd.ExecuteReader();
                if (!dr.Read())
                    throw new Exception("Usuario o Password incorrecto");
                _sesion = Guid.Parse(dr[0].ToString());

                //FrmPrincipal frm = (FrmPrincipal)this.MdiParent;
                //frm.Sesion = id;
                //this.Close();
                
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                dr.Close();
                conn.Close();
            }
        }




    }
}
