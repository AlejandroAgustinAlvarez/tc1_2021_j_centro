﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicacion.ApplicationService
{
    public class UsuarioAppService
    {
        public String ObtenerUsuarioPorId(Guid id)
        {           
            SqlConnectionStringBuilder sb = new SqlConnectionStringBuilder();
            sb.InitialCatalog = "TC1_2021_J";
            sb.DataSource = ".";
            sb.IntegratedSecurity = true;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = sb.ConnectionString;
            IDataReader dr = null;
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = "select username from usuarios where id_usuario=@id";
                cmd.Parameters.AddWithValue("id", id);

                dr = cmd.ExecuteReader();
                if (!dr.Read())
                    throw new Exception("No se encontraron datos del usuario");
                return dr[0].ToString();

            }
            catch (Exception)
            {
                throw ;
            }
            finally
            {
                dr.Close();
                conn.Close();
            }

        }

    }
}
