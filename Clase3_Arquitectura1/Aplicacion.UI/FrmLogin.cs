﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplicacion.UI
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void bnIngresar_Click(object sender, EventArgs e)
        {
            try
            {
                ValidarUsuario(txtUsuario.Text, txtPassword.Text);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ValidarUsuario(string Nombre, string Password)
        {
            if (String.IsNullOrEmpty(Nombre) || String.IsNullOrEmpty(Password))
                throw new Exception("Debe ingresar el Nombre y Password!!");

            SqlConnectionStringBuilder sb = new SqlConnectionStringBuilder();
            sb.InitialCatalog = "TC1_2021_J";
            sb.DataSource = ".";
            sb.IntegratedSecurity = true;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = sb.ConnectionString;
            IDataReader dr = null;
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = "select id_usuario from usuarios where username=@username and password=@password";
                cmd.Parameters.AddWithValue("username", Nombre);
                cmd.Parameters.AddWithValue("password", Password);

                dr = cmd.ExecuteReader();
                if (!dr.Read())
                    throw new Exception("Usuario o Password incorrecto");
                Guid id = Guid.Parse(dr[0].ToString());

                FrmPrincipal frm = (FrmPrincipal)this.MdiParent;
                frm.Sesion = id;
                this.Close();

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                dr.Close();
                conn.Close();
            }
        }
    }
}
