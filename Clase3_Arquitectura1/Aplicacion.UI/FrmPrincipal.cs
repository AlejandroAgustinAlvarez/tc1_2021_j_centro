﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplicacion.UI
{
    public partial class FrmPrincipal : Form
    {

        private Guid _sesion;

        public Guid Sesion 
        {
            get 
            { 
                return _sesion; 
            }
            set 
            { 
                this._sesion = value;
                MostrarUsuario();
                ValidarLog();
            }
        }

        


        public FrmPrincipal()
        {
            InitializeComponent();
            MostrarUsuario();
            ValidarLog();
        }

        private void ValidarLog()
        {
            this.tsMnuIngresar.Enabled = _sesion == Guid.Empty;
            this.tsMnuDesconectar.Enabled = _sesion != Guid.Empty;
        }

        private void MostrarUsuario()
        {
            if(_sesion.Equals(Guid.Empty))
            {
                tsLblStatus.Text = "Sin conexión, ir a Sesión/Ingresar para establecer una conexión.";
            }
            else
            {
                SqlConnectionStringBuilder sb = new SqlConnectionStringBuilder();
                sb.InitialCatalog = "TC1_2021_J";
                sb.DataSource = ".";
                sb.IntegratedSecurity = true;

                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = sb.ConnectionString;
                IDataReader dr = null;
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = conn;
                    cmd.CommandText = "select username from usuarios where id_usuario=@id";
                    cmd.Parameters.AddWithValue("id", _sesion);

                    dr = cmd.ExecuteReader();
                    if (!dr.Read())
                        throw new Exception("No se encontraron datos del usuario");
                    this.tsLblStatus.Text = dr[0].ToString() + " há iniciado sesión";

                }
                catch(Exception e)
                {
                    MessageBox.Show(e.Message);
                }
                finally
                {
                    dr.Close();
                    conn.Close();
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void ingresarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmLogin frmLogin = new FrmLogin();
            frmLogin.MdiParent = this;
            frmLogin.Show();            
        }

        private void desconectarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Sesion = Guid.Empty;
        }
    }
}
