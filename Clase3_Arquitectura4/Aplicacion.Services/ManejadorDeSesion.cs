﻿using Aplicacion.Domain;
using System;

namespace Aplicacion.Services
{
    public static class ManejadorDeSesion
    {
        private static Usuario _sesion;

        public static Usuario Sesion
        {
            get
            {
                return _sesion;
            }
        }

        public static void Login(Usuario usuario)
        {
            _sesion = usuario;
        }

        public static void Logout()
        {
            _sesion = null;
        }

        public static bool IsLogged()
        {
            return _sesion != null;
        }

    }
}
