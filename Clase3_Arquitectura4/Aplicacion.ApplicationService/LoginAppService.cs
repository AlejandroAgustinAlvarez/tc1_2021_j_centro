﻿using Aplicacion.Repository;
using System;
using System.Data;
using System.Data.SqlClient;
using Aplicacion.Domain;
using Aplicacion.Services;

namespace Aplicacion.ApplicationService
{
    public class LoginAppService
    {
        private UsuarioRepository usuarioRepository;

        public LoginAppService()
        {
            usuarioRepository = new UsuarioRepository();
        }

       

        public void CerrarSesion()
        {
            ManejadorDeSesion.Logout();
        }

        public void Login(string Nombre, string Password)        
        {
            if (String.IsNullOrEmpty(Nombre) || String.IsNullOrEmpty(Password))
                throw new Exception("Debe ingresar el Nombre y Password!!");

            try
            {
                Usuario usuario = usuarioRepository.ObtenerUsuarioPorUserNamePassword(Nombre, Password);
                if(usuario == null)
                    throw new Exception("Usuario o Password incorrecto");

                ManejadorDeSesion.Login(usuario);
            }
            catch (Exception)
            {
                throw;
            }
        }




    }
}
