﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aplicacion.Domain;
using Aplicacion.Repository;

namespace Aplicacion.ApplicationService
{
        
    public class UsuarioAppService
    {
    
        private UsuarioRepository usuarioRepository;

        public UsuarioAppService()
        {
            usuarioRepository = new UsuarioRepository();
        }

        public Usuario ObtenerUsuarioPorId(Guid id)
        {
            return usuarioRepository.ObtenerUsuarioPorId(id);
        }

    }
}
