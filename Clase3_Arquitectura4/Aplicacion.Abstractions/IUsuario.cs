﻿using System;

namespace Aplicacion.Abstractions
{
    public interface IEntity
    {       
        public Guid Id { get; set; }
    }
    public interface IUsuario : IEntity
    {
        public string Nombre { get; set; }

        public string Password { get; set; }
    }    
}
