﻿using Aplicacion.ApplicationService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Aplicacion.Domain;
using Aplicacion.Services;

namespace Aplicacion.UI
{
    public partial class FrmPrincipal : Form
    {

        private UsuarioAppService usuarioService;
        private LoginAppService loginService;


        public FrmPrincipal()
        {
            usuarioService = new UsuarioAppService();
            loginService = new LoginAppService();
            InitializeComponent();
            MostrarUsuario();
            ValidarFormulario();
        }

        public void ValidarFormulario()
        {
            this.tsMnuIngresar.Enabled = !ManejadorDeSesion.IsLogged();
            this.tsMnuDesconectar.Enabled = ManejadorDeSesion.IsLogged();
            MostrarUsuario();
        }

        private void MostrarUsuario()
        {
            if(!ManejadorDeSesion.IsLogged())
            {
                tsLblStatus.Text = "Sin conexión, ir a Sesión/Ingresar para establecer una conexión.";
            }
            else
            {
                this.tsLblStatus.Text = ManejadorDeSesion.Sesion.Nombre + " há iniciado sesión";
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void ingresarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmLogin frmLogin = new FrmLogin();
            frmLogin.MdiParent = this;
            frmLogin.Show();            
        }

        private void desconectarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            loginService.CerrarSesion();
            ValidarFormulario();
        }
    }
}
