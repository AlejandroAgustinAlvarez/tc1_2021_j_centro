﻿using Aplicacion.Domain;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Aplicacion.Repository
{
    public class UsuarioRepository
    {

        public Usuario ObtenerUsuarioPorUserNamePassword(string Nombre, string Password)
        {
            SqlConnectionStringBuilder sb = new SqlConnectionStringBuilder();
            sb.InitialCatalog = "TC1_2021_J";
            sb.DataSource = ".";
            sb.IntegratedSecurity = true;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = sb.ConnectionString;
            IDataReader dr = null;
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = "select id_usuario, username, password from usuarios where username=@username and password=@password";
                cmd.Parameters.AddWithValue("username", Nombre);
                cmd.Parameters.AddWithValue("password", Password);

                dr = cmd.ExecuteReader();
                if (!dr.Read()) return null;

                return new Usuario()
                {
                    Id = Guid.Parse(dr["id_usuario"].ToString()),
                    Nombre = dr["username"].ToString(),
                    Password = dr["password"].ToString()
                };
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                dr.Close();
                conn.Close();
            }
        }


        public Usuario ObtenerUsuarioPorId(Guid id)
        {
            SqlConnectionStringBuilder sb = new SqlConnectionStringBuilder();
            sb.InitialCatalog = "TC1_2021_J";
            sb.DataSource = ".";
            sb.IntegratedSecurity = true;

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = sb.ConnectionString;
            IDataReader dr = null;
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = "select id_usuario, username, password from usuarios where id_usuario=@id";
                cmd.Parameters.AddWithValue("id", id);

                dr = cmd.ExecuteReader();
                if (!dr.Read()) return null;

                return new Usuario()
                {
                    Id = Guid.Parse(dr["id_usuario"].ToString()),
                    Nombre = dr["username"].ToString(),
                    Password = dr["password"].ToString()
                };

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                dr.Close();
                conn.Close();
            }

        }


    }
}
