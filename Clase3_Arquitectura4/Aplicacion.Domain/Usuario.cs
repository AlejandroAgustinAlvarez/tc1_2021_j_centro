﻿using Aplicacion.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicacion.Domain
{
    public class Usuario : Entity, IUsuario
    {

        public string Nombre { get; set; }

        public String Password { get; set; }

    }
}
